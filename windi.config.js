import { defineConfig } from "windicss/helpers"
// import colors from "windicss/colors";
// import plugin from "windicss/plugin";

export default defineConfig({
  darkMode: "class", // or 'media'
  theme: {

    colors: {
      white: "#FFFFFF",

      "black-400": "#444444",
      "black-300": "#5C5C5C",
      "black-200": "#9E9E9E", // #989898
      "black-100": "#B3B3B3", // #C9C9C9
      "black-50": "#E3E3E3", // #F0F0F0
      "black-10": "#f8f8f8",

      "blue-600": "#2C3D78",
      "blue-550": "#2445B9",
      "blue-500": "#0B41C1", // #2141C1
      "blue-400": "#305BD8",
      "blue-300": "#3262F4",
      "blue-200": "#3D65D8",
      "blue-100": "#83A8FF",

      "green-700": "#43C3C6",
      "green-600": "#4CCB9E", // #1DC198
      "green-500": "#86EFB2",
      "green-400": "#6BFBCE",

      "purple-300": "#CB89FF",
      "purple-400": "#5545A7",

      "red-400": "#EB3354",
    },
    fontSize: {
      "5xl": ["46px", "69px"],
      "4xl": ["36px", "54px"],
      "3xl": ["32px", "48px"],
      "2xl": ["26px", "39px"],
      xl: ["24px", "36px"],
      lg: ["20px", "30px"],
      base: ["18px", "27px"],
      tiny: ["16px", "24px"],
      sm: ["14px", "21px"],
      xs: ["12px", "18px"],
      icon24: ["24px"],
      icon16: ["16px"],
    },
    filter: {
      none: 'none',
      grayscale: 'grayscale(1)',
      invert: 'invert(1)',
      sepia: 'sepia(1)',
    },
    extend: {
      screens: {
        tablet: "768px",
        "large-tablet": "1024px",
        desktop: "1280px",
        "large-desktop": "1536px",
      },
      fontFamily: {
        poppins: "Poppins",
      },
      lineClamp: {
        sm: "3",
        lg: "10",
      },
    },
  },
  variants: {
    filter: ['responsive'],
  },
  // plugins: [
  //   plugin(({ addUtilities }) => {
  //     const newUtilities = {
  //       ".skew-10deg": {
  //         transform: "skewY(-10deg)"
  //       },
  //       ".skew-15deg": {
  //         transform: "skewY(-15deg)"
  //       }
  //     };
  //     addUtilities(newUtilities);
  //   }),
  //   plugin(({ addComponents }) => {
  //     const buttons = {
  //       ".btn": {
  //         padding: ".5rem 1rem",
  //         borderRadius: ".25rem",
  //         fontWeight: "600"
  //       },
  //       ".btn-blue": {
  //         backgroundColor: "#3490dc",
  //         color: "#fff",
  //         "&:hover": {
  //           backgroundColor: "#2779bd"
  //         }
  //       },
  //       ".btn-red": {
  //         backgroundColor: "#e3342f",
  //         color: "#fff",
  //         "&:hover": {
  //           backgroundColor: "#cc1f1a"
  //         }
  //       }
  //     };
  //     addComponents(buttons);
  //   }),
  plugins: [require("windicss/plugin/line-clamp")],
  //   require("windicss/plugin/filters"),
  //   require("windicss/plugin/forms"),
  //   require("windicss/plugin/aspect-ratio"),
  //   require("windicss/plugin/line-clamp"),
  //   require("windicss/plugin/typography")({
  //     modifiers: ["DEFAULT", "sm", "lg", "red"]
  //   })
  // ]
})
