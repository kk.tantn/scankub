import { Menu, Submenu, MenuItem, Dialog } from "element-ui"
import Vue from "vue"
Vue.use(Menu)
Vue.use(Dialog)
Vue.use(Submenu)
Vue.use(MenuItem)
